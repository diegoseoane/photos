const coolImages = require('cool-images');
const fs = require('fs');
const moment = require('moment');
const constants = require('./constants.js');

let tenImages = coolImages.many(600, 800, 10);

function printUrl(img) {
    for (image of img){
        let today = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        let data = `${today} ${image} \n`
        fs.writeFileSync(constants.logFile, data,{flag: 'a'});
    }
}

printUrl(tenImages);
